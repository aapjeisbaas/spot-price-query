#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo "No insance type chosen, hold on I'll fetch the possible options."
    aws ec2 describe-instance-types --filter Name=supported-usage-class,Values=spot --query "InstanceTypes[].InstanceType" | jq 'sort []' -r
    echo ""
    echo "No insance type chosen"
    echo "Example usage: $0 t4g.nano"
    exit 0
fi

REGIONS=$(aws ec2 describe-regions --query "Regions[].{Name:RegionName}" --output text)

epoch_time_now=$(date +%s)
epoch_time_before=$(echo "$epoch_time_now - 3600" | bc)

datestamp_now=$(date -u +"%Y-%m-%dT%H:%M:%SZ" -d @$epoch_time_now)
datestamp_before=$(date -u +"%Y-%m-%dT%H:%M:%SZ" -d @$epoch_time_before)

for REGION in $REGIONS; do
  echo "${REGION} $(aws ec2 describe-spot-price-history --instance-types $1 --product-description "Linux/UNIX (Amazon VPC)" --start-time $datestamp_before --end-time $datestamp_now --region $REGION | jq '.SpotPriceHistory [0] .SpotPrice' -r)"
done | sort -h -k 2 | grep -ve null

