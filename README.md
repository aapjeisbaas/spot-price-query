
## Requirements

- **jq**
- **aws cli** (v1 or v2 both will work)
- **bash**
- [Some gnu core utils](http://www.maizure.org/projects/decoded-gnu-coreutils/)


## Usage
```
$ ./query.sh t3a.nano
ap-south-1 0.000900
eu-west-1 0.001500
ca-central-1 0.001600
eu-west-2 0.001600
eu-west-3 0.001600
ap-northeast-2 0.001800
ap-southeast-1 0.001800
ap-southeast-2 0.001800
us-east-1 0.001900
ap-northeast-1 0.002000
sa-east-1 0.002300
us-west-1 0.002400
eu-central-1 0.003000
us-west-2 0.004200
us-east-2 0.004500
```

## Power usage 🤘
Maybe you want to run on metal and need to find the cheapest metal you can find:
```
$ for type in $(./query.sh |grep metal) ; do echo $type ; ./query.sh $type ; echo ""; done
a1.metal
us-east-2 0.078800
ap-south-1 0.109500
ap-southeast-1 0.129100
us-west-2 0.133500
us-east-1 0.134300
eu-central-1 0.137500
ap-northeast-1 0.137900
eu-west-1 0.142400
ap-southeast-2 0.142900

c5.metal
us-east-2 0.912200
eu-north-1 1.310400
ap-south-1 1.323800
ca-central-1 1.387700
ap-northeast-2 1.438400
...
...
...
```

## You need even more power? 🛠️ 🦾
Filter the instance types with aws cli and use the aoutput to search for pricing
```
# get all bare metal spot capable machines:
$ aws ec2 describe-instance-types --filter Name=supported-usage-class,Values=spot Name=bare-metal,Values=true --query "InstanceTypes[].InstanceType" | jq 'sort []' -r
a1.metal
c5.metal
c5d.metal
c5n.metal
c6g.metal
c6gd.metal
g4dn.metal
i3.metal
i3en.metal
m5.metal
m5d.metal
m5dn.metal
m5zn.metal
m6g.metal
m6gd.metal
r5.metal
r5d.metal
r5dn.metal
r6g.metal
r6gd.metal
x2gd.metal
z1d.metal

# get only spot metal arm64 machines
$ aws ec2 describe-instance-types --filter Name=supported-usage-class,Values=spot Name=bare-metal,Values=true Name=processor-info.supported-architecture,Values=arm64 --query "InstanceTypes[].InstanceType" | jq 'sort []' -r 
a1.metal
c6g.metal
c6gd.metal
m6g.metal
m6gd.metal
r6g.metal
r6gd.metal
x2gd.metal

# and loop that into the query

$ for type in $(aws ec2 describe-instance-types --filter Name=supported-usage-class,Values=spot Name=bare-metal,Values=true Name=processor-info.supported-architecture,Values=arm64 --query "InstanceTypes[].InstanceType" | jq 'sort []' -r) ; do echo $type ; ./query.sh $type ; echo ""; done

a1.metal
us-east-2 0.078800
ap-south-1 0.109500
ap-southeast-1 0.129100
us-west-2 0.133500
us-east-1 0.134300
eu-central-1 0.137500
ap-northeast-1 0.137900
eu-west-1 0.142400
ap-southeast-2 0.142900

c6g.metal
us-east-2 0.638500
ap-south-1 0.887100
eu-north-1 0.917300
ca-central-1 0.971400
ap-northeast-2 1.006900
us-west-1 1.034000
ap-southeast-1 1.046400
eu-west-2 1.055700
us-west-2 1.081800
us-east-1 1.088200
ap-northeast-1 1.117800
eu-central-1 1.120200
eu-west-1 1.154300
ap-southeast-2 1.157700
sa-east-1 1.370600

c6gd.metal
...
...
...
```

More instance filter options can be found at: [the aws cli docs pages](https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instance-types.html)